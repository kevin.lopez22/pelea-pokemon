const { boolean } = require("yargs");

function pokemones (nombre, poder, vida, velocidad, defensa){
    this.nombre = nombre;
    this.poder = poder;
    this.vida = vida;
    this.velocidad = velocidad;
    this.defensa = defensa;
}
// se crean los pokemon
let pikachu = new pokemones ("pikachu", 35, 100, 60, 25);
let jigglypuff = new pokemones ("jigglypuff", 45, 100, 55, 15);
let charizard = new pokemones ("charizard", 63 , 100, 50, 20);
//let ekans = new pokemones ("ekans", 40,

function pelea (pokemon1, pokemon2)  {
    console.log(`inicia la pelea entre ${pokemon1.nombre} y ${pokemon2.nombre}`);
    let contador = 1;
    let turno = 1;

    if(pokemon2.velocidad > pokemon1.velocidad) {
        turno = 0;
    }

    while (pokemon1.vida > 0 && pokemon2.vida > 0) {
        console.log(`turno : ${contador}`);
        console.log(`${pokemon1.nombre} ${pokemon1.vida}ps ${pokemon1.poder}pd ${pokemon1.defensa}df  vs`+
        ` ${pokemon2.nombre} ${pokemon2.vida}ps ${pokemon2.poder}pd ${pokemon2.defensa}df`);
        if (turno == 1){
            console.log(`Es el turno del pokemon ${pokemon1.nombre}`);
            let dano = pokemon1.poder - pokemon2.defensa 
            if (dano < 0){
                dano=1;
            }
            pokemon2.vida = pokemon2.vida - dano;
            console.log(`${pokemon1.nombre} ataca a ${pokemon2.nombre} y ` + 
            `le quita ${dano} ps`);

            turno = 0;
        } else {
            console.log(`Es el turno del pokemon ${pokemon2.nombre}`);
            let dano = pokemon2.poder - pokemon1.defensa 
            if (dano < 0){
                dano=1;
            }
            console.log(`${pokemon2.nombre} ataca a ${pokemon1.nombre} y ` + 
            `le quita ${dano} ps`);
            pokemon1.vida = pokemon1.vida - dano;
        
            turno = 1;
        }
        contador = contador + 1;
    }
    if (pokemon1.vida <= 0){
        console.log(`la pelea la gana ${pokemon2.nombre} con ${pokemon2.vida}ps`);
    } else {
        console.log(`la pelea la gana ${pokemon1.nombre} con ${pokemon1.vida}ps`);
    }
}

pelea(pikachu, charizard);